### Typescript Hello World Project
#### Setup 
     1. Clone the repo
   >  `https://gitlab.com/prgti/first_ts_project`
        
     2. Verify
   > `npm -v`
        
     3. Install node packages
   > `npm install`
        
     4. Compile the solution 
   >  `npm run build --silent`
        
     5. Test Hello World
   > `npm run start --silent`
        
Verify Okta login
   >  `aws_on media-nonprod_us-east-1`
   
Verify AWS credentials
   > `aws sts get-caller-identity`
    
Test ec2-count solution
   > `npm run start-ec2-count --silent`

Test ec2-status solution
   > `node dist/ec2-status/index.js` 
 > Add filter --state or -s to input ec2 state   
        
        
        
   
   
         
