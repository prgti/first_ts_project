//module for reading data from Readable stream one line at a time
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Hi, May I know your name ?", (answer: any) => {
    console.log(`Hello ${answer}`);

    process.exit();
});


