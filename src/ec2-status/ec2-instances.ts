
import * as AWS from "aws-sdk"

const ec2 = new AWS.EC2;


//Add null checks
export async function getEc2Instances(stateList: string[]): Promise<Map<string, AWS.EC2> | null> {
    let ec2InstanceMap: Map<string, AWS.EC2> = new Map<string, AWS.EC2>();
    let params;

    if (stateList.length == 0) params = {};
    else {
        // console.log(stateList);
        params = {
            Filters: [
                {
                    Name: "instance-state-name",
                    Values: stateList
                }
            ]
        };
    }


    //describeInstances() is a callback function
    let data:any = await ec2.describeInstances(params).promise();

    for (let reservation of data.Reservations) {
        for (let instance of reservation.Instances) {
             // let currentId = instance['InstanceId'];
             // console.log('instance id is '+currentId);

            //Prepare a return Map
            ec2InstanceMap.set(instance['InstanceId'], instance);
        }
    }

    return ec2InstanceMap;
}

//Todo: Take it off later
// let state:string[] = ["stopped","running"];
// getEc2Instances(state).then( value => {
//     console.log("Congratulations for your first TS Module" + value);
// });
