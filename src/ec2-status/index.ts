import * as ec2Handler from "./ec2-instances";
import * as program from "commander";

program.option('-s, --state [type]', 'Add optional state');
program.parse(process.argv);

let stateList : string[] = [];
let default_state = "running";

// filter skipped from the command
if (program.state === undefined) {
 // return count of all instances
}

//filter added but no value entered
else if (program.state === true) {
    stateList.push(default_state);
}

//Return what is asked for
else {
    stateList = program.state.split(',');
}


ec2Handler.getEc2Instances(stateList).then( value => {
    if (value != undefined)
        console.log(`Count: ${value.size}`);
});
