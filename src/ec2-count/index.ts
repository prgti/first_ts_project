import * as AWS from 'aws-sdk'
// eslint-disable-next-line no-unused-vars
import {AWSError} from "aws-sdk";

//get region from logged in account
let region = AWS.config.region;
console.log('region env is ' + region);

function getEC2instances(): void {
    let ec2 = new AWS.EC2;
    // let params = {
    //      "Filters": [{'Name': 'availability-zone', 'Values': ['us-east-1a']}]
    // };
    ec2.describeInstances(function (err: AWSError, data: any) {
        if (err) console.log(err);
        else {
            for (let reservation of data.Reservations) {
                for (let instance of reservation.Instances) {
                    //print instance types on screen
                    //script command would sort and print count also
                    console.log(instance.InstanceType);
                }
            }
        }
    });
}

getEC2instances();
